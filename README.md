# Discolytics - python

Discolytics, short for discovery analytics, is a collection of the small
scripts run by the WMF search platform team within the analytics hadoop
cluster. Execution of these is typically scheduled by
[airflow](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/search-platform)

## Running Tests

First build the test image:
```
DOCKER_BUILDKIT=1 docker build --target tox --tag discolytics-tox:latest -f .pipeline/blubber.yaml .
```

Then run it against the embedded source.
```
docker run --rm discolytics-tox:latest
```

Run only one tox environment, such as flake8 linting:
```
docker run --rm discolytics-tox:latest -e flake8
```

Run a single pytest file
```
docker run --rm discolytics-tox:latest -e pytest -- tests/test_hive.py
```

By default the image will reinstall the conda environment on each run which can
be slow for development. The following works on ubuntu focal to mount the
current directory into the instance and reuse the tox environment between runs.
While it is invoked as root it drops those privileges prior to running the test
suite. The root privileges are used to make a user inside the container that
matches the owner of $PWD outside the container.

TODO: mac instructions
```
docker run --rm -u 0:0 -v $PWD:/srv/app:rw discolytics-tox:latest
```

The container additionally supports starting a bash shell with conda and various
environment values pre-initialized:
```
docker run --rm -u 0:0 -v $PWD:/srv/app:rw discolytics-tox:latest bash
```

## Deployment

Releases and versioning are managed by the data-engineering `conda_artifact_repo.yml` ci workflow.  See the
[workflow documentation](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide/Python_Job_Repos)
for how to make a release.

### tl/dr

Release the current version, bump the version number, and build a new
deployable conda environment for the released version:

1. Browse to [CI Pipelines](https://gitlab.wikimedia.org/repos/search-platform/discolytics/-/pipelines?page=1&scope=branches&ref=main).
2. The most recent merge automatically triggered the testing jobs against the
   main branch and will be visible here.
3. On the right side of the run click the play (`▶`) dropdown and select
   `trigger_release`. This won't be available until the tests have completed.
4. On completion of `trigger_release` a new git tag will have been created.
   This new tag will trigger the rest of the release process, visible from the
   gitlab pipelines page with a name similar to `Release version 0.1.0`.
5. Once the release has completed an artifact will be available in the project
   [package registry](https://gitlab.wikimedia.org/repos/search-platform/discolytics/-/packages).
   This artifact is now available for use in the airflow-dags repository.
