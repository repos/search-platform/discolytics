from pyspark.sql import Column, functions as F


def str_to_map(col: Column, str_delimiter: str, kv_delimiter: str) -> Column:
    """Interpret a delimited string as a map

    This was previously found in hive, and was added to spark in 3.5,
    but we don't have it currently. This is a basic reimplementation.

    Example:
        str_to_map(F.lit('k1=v1&k2=v2'), '&', '=')
    Equivalent to:
        F.map_from_arrays(F.array('k1', 'k2'), F.array('v1', 'v2'))
    """
    def extract_key(kv_pair):
        return F.split(kv_pair, kv_delimiter, 2)[0]

    def extract_value(kv_pair):
        return F.split(kv_pair, kv_delimiter, 2)[1]

    kv_pairs = F.split(col, str_delimiter)
    keys = F.transform(kv_pairs, extract_key)
    values = F.transform(kv_pairs, extract_value)
    return F.map_from_arrays(keys, values)


def get_actor_signature(
    ip: Column,
    user_agent: Column,
    accept_language: Column,
    uri_host: Column,
    uri_query: Column,
    x_analytics_map: Column
) -> Column:
    """Calculate an actor fingerprint from webrequest fields

    We tried to use the hive actor signature udf, but spark was complaining. So we
    reimplemented it from the udf source found in analytics/refinery/source on gerrit
    (git hash fb0f7dbf7451b9258770f3aa683b53fff909b4fb).

    The results likely aren't directly comparable, but they do basically the same thing.
    """
    wmfuuid = x_analytics_map['wmfuuid']
    # In a sample of 21M page views no uri_query contains appInstallID
    appInstallID = F.regexp_extract(uri_query, r'(?:^\?|&)appInstallID=(.+)(?:&|$)', 1)

    return F.md5(F.concat(
        ip,
        user_agent[0:200],
        accept_language,
        uri_host,
        F.coalesce(wmfuuid, appInstallID, F.lit(''))
    ))


def sum_bool(cond: Column) -> Column:
    """Sums over a boolean condition. Considers null as 0."""
    return F.sum(F.coalesce(cond.cast('int'), F.lit(0)))
