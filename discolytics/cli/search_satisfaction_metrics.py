"""Generates fulltext satisfaction metrics from SearchSatisfaction events

Aggregates over the SearchSatisfaction events to determine how satisfied users
are with autocomplete and fulltext search. Analysis is limited to desktop
web users, as the events are only collected there.

Fulltext sessions follow the schema definition, which is that a session continues
until the users does not perform a search for 10 minutes.

Output dimensions:
* access_method
    Constant value of 'desktop'. Included to make it clear that this data is limited.
    Perhaps in the future we can include mobile access methods.
* country
    Country the request ip address is from
* os_family
    The family of operating system the user is using, such as iOS or Linux.
* browser_family
    The family of browser the user is using, such as Firefox or Chrome.
* user_edit_bucket
    The number of edits performed by the user, bucketed into five groups.

Fulltext satisfaction metrics:

* num_fulltext_serp
    The number of Special:Search result pages shown
* num_sessions_w_fulltext_serp
    The number of sessions that saw the Special:Search results page
* num_fulltext_serp_w_results
    The number of Special:Search result pages shown with >= 1 result
* num_sessions_w_serp_w_results
    The number of sessions that saw Special;Search results page with >= 1 result
* num_fulltext_serp_w_click
    The number of clicks on Special:Search results
* num_sessions_w_fulltext_serp_w_click
    The number of sessions that clicked a Special:Search result
* num_fulltext_visit_sat
    The number of article page visits from Special:Search where the user
    dwelled long enough to be considered satisfied. Does not include
    virtual page views.
* num_sessions_w_fulltext_visit_sat
    The number of sessions with article page visits from Special:Search
    where the user dwelled long enough to be considered satisfied.
* num_fulltext_serp_w_virtual_pv
    The number of Special:Search result pages which resulted in at least one
    virtual page view.
* num_sessions_w_fulltext_serp_w_virtual_pv
    The number of sessions which registered at least one virtual page view
    from a fulltext result page.
* num_sessions_w_fulltext_abandon_and_virtual_pv
    The number of sessions which registered virtual page views without registering
    any clicks or visit pages.

Autocomplete satisfaction metrics:

Note that autocomplete metrics exclude any autocomplete that occurs on Special:Search,
as those have a different intent and require measuring satisfaction differently.

* num_autocomplete_serp
  The number of pages that displayed autocomplete results
* num_autocomplete_submit
  The number of pages where the autocompleted form was submitted
* num_autocomplete_success
  The number of pages where the user selected an item from the autocomplete dropdown
* num_autocomplete_abandon
  The number of pages where autocomplete results were displayed but the
  form was not submitted.

The following metrics are calculated as a curiosity, but are not generally used:

* num_sessions_w_autocomplete_serp
  The number of fulltext search sessions that displayed autocomplete results
* num_sessions_w_autocomplete_submit
  The number of fulltext search sessions that submitted an autocompleted form
* num_sessions_w_autocomplete_success
  The number of fulltext search sessions where the user selected an item from
  the autocomplete dropdown
* num_sessions_w_autocomplete_abandon
  The number of fulltext search sessions where at least one page displayed
  autocomplete results without submitting the form.
"""
from argparse import ArgumentParser
from discolytics.hive import HivePartitionTimeRange, HivePartitionWriter
from discolytics.spark import sum_bool
from functools import partial
from pyspark.sql import Column, DataFrame, SparkSession, functions as F
import logging
import sys
from typing import Mapping, Sequence


# Minimum time a user must spend on an article to be satisfied
DWELL_THRESHOLD = 10


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('--search-satisfaction', required=True, type=HivePartitionTimeRange.from_spec)
    parser.add_argument('--output', required=True, type=HivePartitionWriter.from_spec)
    return parser


def satisfaction_metrics():
    """Composes properties of a search satisfaction event into desired metrics"""
    source = F.col('event.source')
    action = F.col('event.action')
    # null for non-click events, -1 for user typed query, >= 0 for clicked position.
    click_position = F.col('event.position')
    checkin_time = F.coalesce(F.col('event.checkin'), F.lit(0))
    hits_returned = F.coalesce(F.col('event.hitsReturned'), F.lit(0))

    is_fulltext = source == 'fulltext'
    is_autocomplete = source == 'autocomplete'
    is_serp = action == 'searchResultPage'
    is_click = action == 'click'
    is_checkin = action == 'checkin'
    is_sat_checkin = checkin_time >= DWELL_THRESHOLD
    is_visit = action == 'visitPage'
    is_virtual_pv = action == 'virtualPageView'

    is_fulltext_serp = is_fulltext & is_serp
    is_fulltext_click = is_fulltext & is_click
    is_autocomplete_serp = is_autocomplete & is_serp
    is_autocomplete_click = is_autocomplete & is_click
    # Autocomplete is taking a slightly different definition of
    # satisfied/dissatisfied. We are going to define an autocomplete session as
    # existing within a single page. We then take the overly bold assumption
    # that a user submitting an autocomplete provided value is a success. This
    # results in them going directly to the page they selected.

    # If the user either abandons their search, or submits a search query they
    # provided, we consider that a dissatisfied autocomplete session. This is
    # perhaps both overly generous to go to page, and not generous enough to
    # fulltext search.

    # In part this is done because for some reason there is a 20:1 ratio of
    # clicks:visitPage for autocomplete, when they should be very close to the
    # same number. The checkin information, submitted by the same bits that
    # should have recorded visitPage events, is similarly missing.
    is_autocomplete_success = is_autocomplete_click & F.coalesce(click_position >= 0, F.lit(False))

    return {
        # clicks only happen on result pages, not sure why we dont always get the initial event.
        'fulltext_serp': is_fulltext_serp | is_fulltext_click,
        # Similarly, to click through they must have seen results
        'fulltext_serp_w_results': (is_fulltext_serp & (hits_returned > 0)) | is_fulltext_click,
        'fulltext_click': is_fulltext_click,
        # Similarly, both a visit and a checkin event indicate a visit
        'fulltext_visit': is_fulltext & (is_visit | is_checkin),
        'fulltext_visit_sat': is_fulltext & is_sat_checkin,
        'fulltext_virtual_pv': is_virtual_pv,
        # similarly autocomplete clicks only happen if an autocomplete serp is shown.
        'autocomplete_serp': is_autocomplete_serp | is_autocomplete_click,
        'autocomplete_submit': is_autocomplete_click,
        'autocomplete_success': is_autocomplete_success,
    }


def per_page_agg(df_events: DataFrame, dimensions: Mapping[str, Column]) -> DataFrame:
    """Aggregate over SearchSatisfaction events into a row per page load"""
    # Each property should be a boolean that indicates something about the page
    # being aggregated over. Generates a row per page that indicates, per-property,
    # if any event submitted for that page load had that property.
    return (
        df_events
        .groupBy(
            F.col('normalized_host'),
            F.col('event.searchSessionId'),
            F.col('event.pageViewId'),
        )
        .agg(
            *((sum_bool(prop) > 0).alias(f'is_{name}')
                for name, prop in satisfaction_metrics().items()),
            # Dimensions should be constant during a page load
            *(F.first(col).alias(name) for name, col in dimensions.items())
        )
    )


def per_session_agg(df_per_page: DataFrame, dimensions: Sequence[str]) -> DataFrame:
    """Aggregate over per page rows into per session rows"""
    # Each property should be a boolean that indicates something about the page
    # being aggregated. Generates per-session counts of pages with each property

    fulltext_properties = {
        # Number of fulltext search result pages the session saw
        'serp': F.col('is_fulltext_serp'),
        # Number of fulltext search results pages that had >= 1 result
        'serp_w_results': F.col('is_fulltext_serp_w_results'),
        # Number of fulltext search result pages that had a click
        'serp_w_click': F.col('is_fulltext_click'),
        # Number of result pages the session visited from fulltext search that
        # registered a virtual page view.
        'serp_w_virtual_pv': F.col('is_fulltext_virtual_pv'),
        # Number of pages the session visited from fulltext search
        'visit': F.col('is_fulltext_visit'),
        # Number of pages the session visited from fulltext search that met the
        # dwell time threshold
        'visit_sat': F.col('is_fulltext_visit_sat'),
    }

    # Note that we are grouping on fulltext session ids. For autocomplete we
    # are considering only the events that occur in a single page as the
    # autocomplete session.
    autocomplete_properties = {
        # The number of pages the session saw autocomplete results on
        'serp': F.col('is_autocomplete_serp'),
        # The number of pages the session submitted the autcomplete input on
        'submit': F.col('is_autocomplete_submit'),
        # The number of pages the session submitted an autocomplete provided input on.
        'success': F.col('is_autocomplete_success'),
        # The number of pages the session saw autocomplete results on but never submitted.
        'abandon': F.col('is_autocomplete_serp') & ~F.col('is_autocomplete_submit')
    }

    return (
        df_per_page
        .groupBy(
            F.col('normalized_host'),
            F.col('searchSessionId'),
        )
        .agg(
            *(sum_bool(prop).alias(f'num_fulltext_{name}')
                for name, prop in fulltext_properties.items()),
            # We additionally exclude any autocomplete that occured on a
            # fulltext SERP, as the behaviour is different
            *(sum_bool(~F.col('is_fulltext_serp') & prop).alias(f'num_autocomplete_{name}')
              for name, prop in autocomplete_properties.items()),

            # Dimensions should also be constant within a single session
            *(F.first(F.col(x)).alias(x) for x in dimensions),
        )
    )


def final_agg(df_per_session: DataFrame, dimensions: Sequence[str]) -> DataFrame:
    """Aggregate over per session rows into our final reporting dimensions"""
    # Each property should be an integer that represents the number of times
    # something happened in a session. For each property we will calculate the
    # number of times the property occured across all sessions, and the number
    # of sessions with that property.

    fulltext_properties = {
        # sessions that saw a result page
        'serp': F.col('num_fulltext_serp'),
        # sessions that saw a result page with >= 1 results
        'serp_w_results': F.col('num_fulltext_serp_w_results'),
        # sessions that clicked a result
        'serp_w_click': F.col('num_fulltext_serp_w_click'),
        # sessions that had a satisfied page visit
        'visit_sat': F.col('num_fulltext_visit_sat'),
        # sessions that had a serp with a virtual page view
        'serp_w_virtual_pv': F.col('num_fulltext_serp_w_virtual_pv'),
    }

    autocomplete_properties = {
        'serp': F.col('num_autocomplete_serp'),
        'submit': F.col('num_autocomplete_submit'),
        'success': F.col('num_autocomplete_success'),
        'abandon': F.col('num_autocomplete_abandon'),
    }

    return (
        df_per_session
        .groupBy(
            F.col('normalized_host'),
            *(F.col(x) for x in dimensions),
        )
        .agg(
            *(F.sum(prop).alias(f'num_fulltext_{name}')
                for name, prop in fulltext_properties.items()),
            *(sum_bool(prop > 0).alias(f'num_sessions_w_fulltext_{name}')
                for name, prop in fulltext_properties.items()),
            *(F.sum(prop).alias(f'num_autocomplete_{name}')
                for name, prop in autocomplete_properties.items()),
            # We calculate per-fulltext session autocomplete metrics as a
            # curiosity but don't end up using them. Instead using the per-page
            # definition of an autocomplete session.
            *(sum_bool(prop > 0).alias(f'num_sessions_w_autocomplete_{name}')
                for name, prop in autocomplete_properties.items()),

            # Number of fulltext sessions that didn't have a satisfied visit
            sum_bool(
                (F.col('num_fulltext_serp') > 0)
                & (F.col('num_fulltext_visit_sat') == 0)
            ).alias('num_sessions_w_fulltext_dsat'),

            # number of sessions that didn't click a result link
            sum_bool(
                (F.col('num_fulltext_serp') > 0)
                & (F.col('num_fulltext_serp_w_click') == 0)
                # Event delivery isn't 100% for click events, but to
                # get a page visit they must have clicked something.
                & (F.col('num_fulltext_visit') == 0)
            ).alias('num_sessions_w_fulltext_abandon'),

            # number of sessions that didn't click a result link
            # but did see a virtual pageview on the serp.
            sum_bool(
                (F.col('num_fulltext_serp') > 0)
                & (F.col('num_fulltext_serp_w_click') == 0)
                & (F.col('num_fulltext_visit') == 0)
                & (F.col('num_fulltext_serp_w_virtual_pv') > 0)
            ).alias('num_sessions_w_fulltext_abandon_and_virtual_pv'),
        )
    )


def agg_from_events(df: DataFrame, dimensions: Mapping[str, Column]) -> DataFrame:
    return (
        df
        .where(~F.col('useragent.is_bot'))
        .transform(partial(per_page_agg, dimensions=dimensions))
        .transform(partial(per_session_agg, dimensions=dimensions.keys()))
        .transform(partial(final_agg, dimensions=dimensions.keys()))
    )


def reporting_dimensions() -> Mapping[str, Column]:
    return {
        # Reporting dimensions
        'os_family': F.col('useragent.os_family'),
        'country': F.col('geocoded_data.country'),
        'browser_family':  F.col('useragent.browser_family'),
        'user_edit_bucket': F.col('event.userEditBucket'),
        # Make it explicit to anyone consuming this data that the data
        # collection is limited to desktop.
        'access_method': F.lit('desktop'),
    }


def main(
    search_satisfaction: HivePartitionTimeRange,
    output: HivePartitionWriter,
) -> int:
    spark = SparkSession.builder.getOrCreate()

    df_in = search_satisfaction.read(spark)

    df_out = agg_from_events(df_in, reporting_dimensions())
    output.overwrite_with(df_out.repartition(1))

    return 0


def run_cli() -> int:
    logging.basicConfig(level=logging.INFO)
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == "__main__":
    sys.exit(run_cli())
