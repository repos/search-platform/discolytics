"""Generates usage and pageview metrics from wmf webrequest table

This is primarily a classification task. We process the general webrequest logs
and put together two main pieces of information:

* Request counts for various requests that trigger search. These columns are
  all suffixed _req in the output table. This is particularly important to
  understand usage of cached endpoints that have incomplete backend usage
  metrics.

* Page view attribution counts for search. These columns are all suffixed _pv
  in the output table. These columns aim to count the number of page views that
  can be attributed to search results in their various forms.

Actor fingerprinting is used as a proxy for a daily unique devices count. In a
30 day analysis the daily actor count was ~15% overestimated and had a
correlation of 0.55. It's not a great proxy, but reasonable.

Output dimensions:

* access_method
    The UI used. Either desktop, mobile, or mobile app
* normalized_host
    The site that was visited. Includes sub-properties for language, project,
    and project family.
* country
    Country the request ip address is from.
* browser_family
    The family of web browser the user is using, such as Firefox or Chrome.
* os_family
    The family of operating system the user is using, such as iOS or Linux.

Pageview attribution metrics:

* num_pageviews
    The number of pageviews performed
* num_actors_w_pageviews
    The number of actors performing pageviews
* num_autocomplete_pv
    The number of pageviews that came from autocomplete clickthroughs
* num_actors_w_autocomplete_pv
    The number of actors performing autocomplete pageviews
* num_special_search_pv
    The number of pageviews that came from Special:Search
* num_actors_w_special_search_pv
    The number of actors performing Special:Search pageviews
* num_related_articles_pv
    The number of pageviews that came from Related Articles
* num_actors_w_related_articles_pv
    The number of actors performing Related Articles pageviews
* num_external_search_pv
    The number of pageviews that came from external search engines
* num_actors_w_external_search_pv
    The number of actors performing external search engine pageviews
* num_internal_pv
    The number of internally referred pageviews
* num_actors_w_internal_pv
    The number of actors performing internally referred pageviews
* num_search_pv
    The number of pageviews that came from autocomplete or Special:Search
* num_actors_w_search_pv
    The number of actors performing pageviews from autocomplete or
    Special:Search
* num_discovery_pv
    The number of pageviews that came from autocomplete, Special:Search, or
    Related Artiles
* num_actors_w_discovery_pv:
    The number of actors performing autocomplete, Special:Search, or Related
    Articles pageviews

Request counting metrics:

All request counting metrics are non-overlapping and can be combined. Note that
this only holds for request counts, adding the actor counts will double-count
actors.

* num_go_to_req:
    The number of Special:Search requests returning go-to-page responses. Note that
    mobile web skips this step and sends users directly to the page. num_autocomplete_pv
    provides a more complete look at go-to page.
* num_actors_w_go_to_req
    The number of actors receiving go-to-page responses from Special:Search
* num_serp_req:
    The number of Special:Search requests returning fulltext search results.
* num_actors_w_serp_req:
    The number of actors receving fulltext search results on Special:Search
* num_ac_req:
    The number of autocomplete requests received
* num_actors_w_ac_req:
    The number of actors performing autocomplete requests.
* num_morelike_req:
    The number of related articles (more like) requests
* num_actors_w_morelike_req:
    The number of actors performing related articles (more like) requests
* num_other_api_fulltext_req:
    The number of api search requests that did not match any other *_req
    classification
* num_actors_w_other_api_fulltext_req:
    The number of actors with api search requests that did not match any other
    *_req classification
"""
from argparse import ArgumentParser
from discolytics.hive import HivePartitionTimeRange, HivePartitionWriter
from discolytics.spark import get_actor_signature, str_to_map, sum_bool
from functools import reduce
import itertools
from pyspark.sql import DataFrame, SparkSession, functions as F
import logging
import sys


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('--webrequests', required=True, type=HivePartitionTimeRange.from_spec)
    parser.add_argument('--output', required=True, type=HivePartitionWriter.from_spec)
    parser.add_argument(
        '--validate', default=False, action='store_true',
        help='Run a validation routine against the webrequests. Does not write to output.')
    return parser


def webrequest_metrics():
    """Composes various properties of a webrequest into desired metrics"""
    is_pageview = F.col('is_pageview')
    is_api = F.col('uri_path') == '/w/api.php'
    is_index_php = F.col('uri_path') == '/w/index.php'
    is_200_ok = F.col('http_status') == '200'
    is_redirect = F.col('http_status').startswith('3')

    has_external_search_referer = F.col('referer_class') == 'external (search engine)'
    has_internal_referer = F.col('referer_class') == 'internal'
    # Token attached to a result url (ac & fulltext) which is seen
    # by the page-view clicked through to as part of the referer.
    has_referer_token = F.col('referer').contains('searchToken=')

    # The wprov's come from desktop search satisfaction tracking in wikimedia events
    wprov = F.coalesce(F.col('x_analytics_map')['wprov'], F.lit(''))
    has_fulltext_wprov = wprov.startswith('srpw1_')
    has_autocomplete_wprov = wprov.startswith('acrw1_')
    has_search_wprov = has_fulltext_wprov | has_autocomplete_wprov
    # Substr from 2 because spark is 1-indexed and uri_query has a leading '?'
    clean_uri_query = F.col('uri_query').substr(F.lit(2), F.length(F.col('uri_query')))
    qs = str_to_map(clean_uri_query, '&', '=')

    # For SERP's we can look at x_analytics_map['special'], but that doesn't get populated
    # when Special:Search issues a redirect (go to page).
    # We don't have other great options to detect go to page requests. The url's are translated
    # into the wiki language and tedious to detect. For the longest time this query parameter
    # triggered Special:Search no matter what page you were on. That is no longer the case, but
    # it means there aren't other historical use cases of this and it's probably a resonable proxy.
    has_search_qs = qs['search'].isNotNull()
    special_page = F.coalesce(F.col('x_analytics_map')['special'], F.lit(''))
    is_special_search = special_page.isin('Search', 'MediaSearch')

    # General request classifiers for counting request types

    # when a user submits an autocomplete and goes directly to a page
    # Note that this does not encompas all instances of "go to page", only those that
    # travel through Special:Search. This varies by skin. We do not attempt to count
    # them here as the intent of this section is to count requests.
    is_special_search_go_req = is_index_php & has_search_qs & is_redirect
    # when a user submits an autocomplete and goes to the results page
    is_special_search_results_req = is_special_search & is_200_ok

    # UI's that use autocomplete on titles (VE, etc.) are mixed in here somewhere
    # between opensearch and prefixsearch.
    is_opensearch_req = (
        is_api & is_200_ok
        & F.coalesce(qs['action'] == 'opensearch', F.lit(False))
    )
    is_prefixsearch_req = (
        is_api & is_200_ok
        & F.coalesce(qs['action'] == 'query', F.lit(False))
        & (
            F.coalesce(qs['list'] == 'prefixsearch', F.lit(False))
            | F.coalesce(qs['generator'] == 'prefixsearch', F.lit(False))
        )
    )
    is_rest_title_completion_req = is_200_ok & (F.col('uri_path') == '/w/rest.php/v1/search/title')
    is_autocomplete_req = is_opensearch_req | is_prefixsearch_req | is_rest_title_completion_req

    is_morelike_req = (
        is_api & is_200_ok
        & F.coalesce(qs['action'] == 'query', F.lit(False))
        & F.coalesce(qs['generator'] == 'search', F.lit(False))
        & F.coalesce(qs['gsrsearch'].startswith('morelike'), F.lit(False))
    )

    # Includes mobile apps primary search endpoint, not sure about other use cases
    is_other_api_search_req = (
        is_api & is_200_ok
        & ~is_morelike_req
        # mobile app search invokes prefix and regular search in the same request, but
        # regular search is only for providing query suggestions here. Exclude those requests
        # from the `other` category as they are already counted above
        & ~is_prefixsearch_req
        & F.coalesce(qs['action'] == 'query', F.lit(False))
        & (
            F.coalesce(qs['list'] == 'search', F.lit(False))
            | F.coalesce(qs['generator'] == 'search', F.lit(False))
        )
    )

    is_special_search_pv = is_pageview & has_fulltext_wprov
    is_desktop_autocomplete_pv = is_redirect & has_autocomplete_wprov
    is_minerva_autocomplete_pv = is_pageview & has_referer_token & ~has_search_wprov
    is_autocomplete_pv = is_desktop_autocomplete_pv | is_minerva_autocomplete_pv
    is_related_articles_pv = is_pageview & (wprov == 'rarw1')
    is_external_search_pv = is_pageview & has_external_search_referer
    is_internal_pv = is_pageview & has_internal_referer
    is_search_pv = is_special_search_pv | is_autocomplete_pv
    is_discovery_pv = is_search_pv | is_related_articles_pv

    return {
        'num_pageviews': is_pageview,
        # Specifically for attributing pageviews to sources. These are all
        # meaningful as being the source of the pageviews. Each of these _pv
        # values are distinct. They can be added and still be a count of
        # distinct pageviews. But the actor counts cannot be added.
        'num_autocomplete_pv': is_autocomplete_pv,
        'num_special_search_pv': is_special_search_pv,
        'num_related_articles_pv': is_related_articles_pv,
        'num_external_search_pv': is_external_search_pv,

        # Except the following three, they overlap with the search pv counts.
        # While in some cases the overall counts could be added in
        # post-processing, the related actor counts can only be calculated now
        # or we would double count overlaps.
        'num_internal_pv': is_internal_pv,
        'num_search_pv': is_search_pv,
        'num_discovery_pv': is_discovery_pv,

        # The request counting metrics are also distinct, they can be added
        # and still be a count of incoming requests.
        'num_go_to_req': is_special_search_go_req,
        'num_serp_req': is_special_search_results_req,
        'num_ac_req': is_autocomplete_req,
        'num_morelike_req': is_morelike_req,
        'num_other_api_fulltext_req': is_other_api_search_req,
    }


def agg_from_webrequests(df: DataFrame) -> DataFrame:
    dimensions = {
        'access_method': F.col('access_method'),
        'normalized_host': F.col('normalized_host'),
        'country': F.col('geocoded_data.country'),
        'browser_family': F.col('user_agent_map')['browser_family'],
        'os_family': F.col('user_agent_map')['os_family']
    }
    metrics = webrequest_metrics()
    is_user = F.col('agent_type') == 'user'

    return (
        df
        .where(is_user & reduce(lambda a, b: a | b, metrics.values()))
        # first perform per-actor aggregation
        .groupBy(get_actor_signature(
            F.col('ip'), F.col('user_agent'), F.col('accept_language'),
            F.col('uri_host'), F.col('uri_query'), F.col('x_analytics_map')
        ).alias('actor_signature'))
        .agg(
            # Most dimensions are part of actor signature, should only be a single value.
            *(F.first(dim).alias(name) for name, dim in dimensions.items()),
            *(sum_bool(metric).alias(name) for name, metric in metrics.items())
        )
        # Second pre-aggregation to shrink data to manageable sizes and generate
        # both overall metrics and number of actors contributing to each metric.
        .groupBy(*dimensions.keys())
        .agg(
            # Number of actors contributing to each metric
            *(sum_bool(F.col(x) > 0).alias(x.replace('num_', 'num_actors_w_'))
              for x in metrics.keys()),
            # Overall metric counts, not per-actor
            *(F.sum(F.col(x)).alias(x) for x in metrics.keys())
        )
    )


def pairs(sequence):
    return [
        (a, b)
        for a, b in itertools.product(sequence, sequence)
        # Emit only (a,b) and not (b,a) or (a,a)
        if a < b
    ]


def pair_distinct_metrics(distinct_cols):
    # The general idea is any pair of distinct values & together must be 0
    for (a_name, a_metric), (b_name, b_metric) in pairs(distinct_cols.items()):
        name = f'{a_name}_and_{b_name}'
        is_bad = (a_metric & b_metric) != 0
        yield sum_bool(is_bad).alias(name)


def validate_assumptions(df):
    metrics = webrequest_metrics()

    result = (
        df
        .where(F.col('agent_type') == 'user')
        .select(
            F.sum(F.lit(1)).alias('num_rows'),
            *pair_distinct_metrics({
                'ac_pv': metrics['is_autocomplete_pv'],
                'ss_pv': metrics['is_special_search_pv'],
                'ra_pv': metrics['is_related_articles_pv'],
                'ext_pv': metrics['is_external_search_pv'],
            }),
            *pair_distinct_metrics({
                'go': metrics['is_special_search_go_req'],
                'serp': metrics['is_special_search_results_req'],
                'ac': metrics['is_autocomplete_req'],
                'morelike': metrics['is_morelike_req'],
                'other': metrics['is_other_api_search_req'],
            }),
        )
        .toPandas()
    )
    num_rows = result['num_rows'].iloc[0]
    del result['num_rows']

    t = result.transpose()
    assert list(t.columns) == [0]
    violations = t[t[0] > 0]
    return num_rows, violations


def main(
    webrequests: HivePartitionTimeRange,
    validate: bool,
    output: HivePartitionWriter,
) -> int:
    spark = SparkSession.builder.getOrCreate()

    df_in = webrequests.read(spark)
    if validate:
        rows_checked, violations = validate_assumptions(df_in)
        print(f'Rows checked: {rows_checked}')
        if violations:
            print(violations)
        else:
            print('All good!')
        return 1 if violations else 0

    df_out = agg_from_webrequests(df_in)
    output.overwrite_with(df_out.repartition(1))

    return 0


def run_cli() -> int:
    logging.basicConfig(level=logging.INFO)
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == "__main__":
    sys.exit(run_cli())
