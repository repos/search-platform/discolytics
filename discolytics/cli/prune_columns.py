"""
Simple script that converts one dataset into another allowing to "remove" fields and repartition the
dataset. Main goal is to prune a set of columns to generate a smaller dataset.
What to do is stored in the CONVERSIONS dictionary, the caller must explicitly set what convertion
to use with the --profile flag.
"""
import sys
from argparse import ArgumentParser, Action
from typing import Iterable, NamedTuple, Optional

from pyspark.sql import SparkSession, DataFrame

from discolytics.hive import HivePartition, HivePartitionWriter


class ConversionProfile(NamedTuple):
    removed_fields: Iterable[str]
    num_partitions: Optional[int]


CONVERSIONS = {
    # Extract a subset of the cirrus index that contains only non-content fields.
    # So far content fields are manually identified using stats extracted from production wikis.
    # full stats:
    # https://docs.google.com/spreadsheets/d/1NTOhfw5pRPZBxZ017G-SblfSvvJ7hLIpSNtA_IUO2iQ/edit?usp=sharing
    'cirrus_index_without_content': ConversionProfile(
        removed_fields=[
            'text',  # max size found 4403692
            'source_text',  # max size found 4553441
            'auxiliary_text',  # max size found 1868049
            'opening_text',  # max size found 1720439
            'external_link',  # max size found 2092186
            'file_text',
            'labels',
            'descriptions',
            'statement_keywords',  # max size found: 124456
            'extra_source',
            'labels_all',
            'lexeme_forms'
        ],
        num_partitions=200
    )
}


class StoreConversionAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, CONVERSIONS.get(values))


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('--profile', required=True, choices=CONVERSIONS.keys(),
                        action=StoreConversionAction)
    parser.add_argument('--input-partition', required=True, type=HivePartition.from_spec)
    parser.add_argument('--output-partition', required=True, type=HivePartitionWriter.from_spec)
    return parser


def main(input_partition: HivePartition, output_partition: HivePartitionWriter,
         profile: ConversionProfile):
    spark = SparkSession.builder.getOrCreate()
    source_df = input_partition.read(spark)
    # drop partition fields present in the source but provided in the output partition specs
    # the script should be launched with exactly with the same partition on both ends, but
    # we should trust the caller and remove them from the source
    filtered_df = drop_conflicting_source_partition_fields(output_partition, source_df)
    filtered_df = filter_content_fields(filtered_df, profile.removed_fields)
    if profile.num_partitions is not None:
        filtered_df = filtered_df.repartition(profile.num_partitions)
    output_partition.overwrite_with(filtered_df)
    return 0


def drop_conflicting_source_partition_fields(output_partition: HivePartitionWriter,
                                             source_df: DataFrame):
    return source_df.drop(*list(output_partition.partition.partition_spec.keys()))


def filter_content_fields(source: DataFrame, removed_fields: Iterable[str]) -> DataFrame:
    return source.drop(*removed_fields)


def run_cli() -> int:
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == "__main__":
    sys.exit(run_cli())
