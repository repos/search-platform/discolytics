#!/usr/bin/env python3
"""
Automatically deletes old directories following the naming scheme date=YYYYMMDD
or bare YYYYMMDD.
"""


from __future__ import annotations
from argparse import ArgumentParser
from datetime import datetime, timedelta
from pyarrow.fs import FileInfo, FileType, FileSelector, FileSystem
import logging
import os
import re
import sys
from typing import Iterator, Sequence
import urllib.parse
from urllib.parse import urljoin


# Necessary for urljoin to handle hdfs:// uri's appropriately
urllib.parse.uses_relative.append('hdfs')
urllib.parse.uses_netloc.append('hdfs')

# Needed for pyarrow to find hdfs
if 'CLASSPATH' not in os.environ:
    # fails silently if hdfs is not available, useful for the test suite
    os.environ['CLASSPATH'] = os.popen('hdfs classpath --glob').read().strip()

logger = logging.getLogger()
logger.setLevel(logging.NOTSET)


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('-d', '--dry-run', default=False, action='store_true')
    parser.add_argument('-a', '--delete-after-days', dest='max_age', default=timedelta(days=30),
                        type=lambda x: timedelta(days=int(x)))
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('directories', metavar='PATH', nargs='*', type=Directory.from_uri,
                        help='uri paths containing date=YYYYMMDD or bare YYYYMMDD '
                             'directories to delete. Ex: hdfs://my-cluster/foo')
    return parser


class Directory:
    def __init__(self, fs: FileSystem, file_info: FileInfo, uri: str):
        self.fs = fs
        assert file_info.type == FileType.Directory
        self.file_info = file_info
        self.uri = uri

    @classmethod
    def from_uri(cls, uri: str) -> Directory:
        fs, path = FileSystem.from_uri(uri)
        fi = fs.get_file_info(path)
        if fi.type != FileType.Directory:
            raise Exception(f'{uri} is not a directory! Found: {fi.type}')
        return cls(fs, fi, uri)

    def subdirs(self) -> Iterator[Directory]:
        selector = FileSelector(self.file_info.path)
        for file_info in self.fs.get_file_info(selector):
            uri = urljoin(self.uri, file_info.path)
            if file_info.type == FileType.Directory:
                yield Directory(self.fs, file_info, uri)
            else:
                logging.debug('Ignoring non-directory: {uri}')

    def delete(self) -> None:
        self.fs.delete_dir(self.file_info.path)


DATE_PATTERN = re.compile(r'(?:date=)?(\d{8})')


def subdirs_to_prune(source: Directory, prune_before: datetime) -> Iterator[Directory]:
    for subdir in source.subdirs():
        match = DATE_PATTERN.fullmatch(subdir.file_info.base_name)
        if not match:
            logger.debug(f'Ignoring non-matching subdir: {subdir.uri}')
            continue

        try:
            dt = datetime.strptime(match.group(1), '%Y%m%d')
        except ValueError:
            logger.error(f'Ignoring subdir with invalid date: {subdir.uri}')
            continue

        if dt > prune_before:
            logger.debug(f'Ignoring subdir within max_age: {subdir.uri}')
            continue

        yield subdir


def main(
    dry_run: bool,
    max_age: timedelta,
    directories: Sequence[Directory],
    verbose: bool,
) -> int:
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if dry_run:
        logging.info("Running in DRY-RUN mode")

    prune_before = datetime.now() - max_age
    logging.debug(f'Will prune directories dated prior to {prune_before.isoformat(timespec="minutes")}')

    for directory in directories:
        logging.debug(f'Looking for directories to delete in {directory.uri}')
        for target in subdirs_to_prune(directory, prune_before):
            logging.info(f'DELETE: {target.uri}')
            if not dry_run:
                target.delete()
    return 0


def run_cli() -> int:
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == '__main__':
    sys.exit(run_cli())
