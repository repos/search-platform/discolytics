""" Script to check a few inconsistency patterns between the page table and CirrusSearch index
content.

It produces all detailed inconsistencies to hive and stats per wiki and type of inconsistency
to prometheus via the push gateway.

Because cirrus index and mysql snapshots are not occurring at the same time some detection
are limited to the changes made at the time the oldest snapshot was taken.

It is very likely that some discrepancies are due to broken pages:
- the page fails to render
- the page is considered a redirect in the page table but not from the content or vice versa
  (happens frequently on css/javascript content model)
- the title is considered a "BadTitle"

These false positives are not yet identified and excluded. It might be interesting to detect those
(if they constantly appear in the output it probably means that the Sanitizer is unable to fix them)
and ponder whether these pages can be fixed or removed from their host wiki or not.

The script currently checks these inconsistencies:
- in_cirrus_but_not_in_mysql: a page in cirrus but not in mysql
- in_mysql_but_not_in_cirrus: a page in mysql but not in cirrus
- redirect_in_cirrus: a redirect that is in cirrus (should not cirrus only indexes target pages)
- revision_mismatch: a page in mysql and cirrus but where the revision do not match

It is missing few checks:
- make sure the page is in the proper index (content vs general)
- does the indexed content reflects the latest rendering of the page, might be particularly hard to
  detect given that elasticsearch might ignore some updates (noop)
"""
import logging
import sys
from argparse import ArgumentParser
from datetime import timedelta
from typing import Optional

from prometheus_client import Gauge, CollectorRegistry, push_to_gateway
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import col, to_timestamp, lit, sum, when, unix_timestamp, from_unixtime, \
    max as _max, count

from discolytics.hive import HivePartition, HivePartitionWriter

# approximate time it takes to dump the source datasets to hive
DUMP_TIME_ADJ = timedelta(hours=6)

# The output dataset is hopefully pretty small
NUM_PARTITIONS = 1


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('--push-gw-endpoint', dest='push_gw_endpoint',
                        required=False, help='Prometheus push gateway endpoint')
    parser.add_argument('--index-partition-before',
                        help="Partition holding the index as fetched before the mysql state",
                        required=True, type=HivePartition.from_spec)
    parser.add_argument('--index-partition-after',
                        help="Partition holding the index as fetched after the mysql state",
                        required=True, type=HivePartition.from_spec)
    parser.add_argument('--page-table-partition',
                        help="Partition holding the page table data",
                        required=True, type=HivePartition.from_spec)
    parser.add_argument('--output',
                        help="Partition holding the details of the inconsistencies",
                        required=True, type=HivePartitionWriter.from_spec)
    return parser


def main(
        index_partition_before: HivePartition,
        index_partition_after: HivePartition,
        page_table_partition: HivePartition,
        output: HivePartitionWriter,
        push_gw_endpoint: Optional[str]

) -> int:
    spark = SparkSession.builder.getOrCreate()
    try:
        if index_partition_before.partition('cirrus_replica') != index_partition_after.partition(
                "cirrus_replica"):
            raise ValueError("{} and {} must use the same cirrus_replica column")
    except KeyError:
        raise ValueError("cirrus_replica must be set in partition columns")
    df_cirrus_before = select_cirrus_fields(index_partition_before.read(spark).cache())
    df_cirrus_after = select_cirrus_fields(index_partition_after.read(spark)).cache()
    df_page_table = select_page_table_fields(page_table_partition.read(spark)).cache()
    page_counts = page_counts_per_wiki_and_content_model(df_page_table)

    inconsistencies = (
        in_cirrus_but_not_in_mysql(df_cirrus_after, df_page_table)
        .union(in_mysql_but_not_in_cirrus(df_cirrus_before, df_page_table))
        .union(redirect_in_cirrus(df_cirrus_after, df_page_table))
        .union(revision_mismatch(df_cirrus_after, df_page_table))
        .join(page_counts, ['wiki', 'content_model'], 'inner')
        .withColumn("timestamp", col("timestamp").cast("string"))
        .withColumn("cirrus_replica", lit(index_partition_before.partition('cirrus_replica')))
        .coalesce(NUM_PARTITIONS))

    if push_gw_endpoint is not None:
        inconsistencies = inconsistencies.cache()

    output.overwrite_with(inconsistencies)

    if push_gw_endpoint is not None:
        counts = aggregate(inconsistencies,
                           index_partition_before.partition('cirrus_replica'))
        collect_and_push_to_prometheus(push_gw_endpoint, counts)
    return 0


def collect_and_push_to_prometheus(endpoint: str, inconsistencies: DataFrame):
    registry = CollectorRegistry()
    collect_metrics(inconsistencies, registry)
    push_to_gateway(endpoint,
                    job='check_cirrus_index_consistency',
                    registry=registry)


def collect_metrics(inconsistencies: DataFrame, registry: CollectorRegistry):
    inconsistencies_gauge = Gauge(
        'cirrussearch_content_inconsistency',
        'number of inconsistencies found between elasticsearch and mysql',
        ['inconsistency_type', 'content_model', 'cirrus_replica'],
        registry=registry
    )
    for r in inconsistencies.collect():
        (inconsistencies_gauge
         .labels(content_model=r['content_model'],
                 inconsistency_type=r['inconsistency_type'],
                 cirrus_replica=r['cirrus_replica'])
         .set(r['count']))


def select_cirrus_fields(cirrus_df: DataFrame) -> DataFrame:
    return cirrus_df.select(
        col("wiki"),
        col("page_id"),
        col("namespace"),
        col("version").alias("revision"),
        to_timestamp(col("timestamp"), "yyyy-MM-dd'T'HH:mm:ss[.SSSSSS]'Z'").alias("timestamp"),
        col("content_model"))


def select_page_table_fields(df_page_table: DataFrame) -> DataFrame:
    return df_page_table.select(
        col("wiki_db").alias("wiki"),
        col("page_id"),
        col("page_namespace").alias("namespace"),
        col("page_latest").alias("revision"),
        to_timestamp(col("page_touched"), "yyyyMMddHHmmss").alias("timestamp"),
        col("page_content_model").alias("content_model"),
        col("page_is_redirect")
    )


def page_counts_per_wiki_and_content_model(page_table_df: DataFrame) -> DataFrame:
    """
    page and redirect counts per wiki.
    :param page_table_df:
    :return: a DataFrame with wiki, num_pages, num_redirects
    """
    return (page_table_df
            .groupBy('wiki', 'content_model')
            .agg(sum(when(page_table_df["page_is_redirect"] == lit(False),
                          lit(1)).otherwise(lit(0))).alias("num_pages"),
                 sum(when(page_table_df["page_is_redirect"] == lit(True),
                          lit(1)).otherwise(lit(0))).alias("num_redirects")))


def in_cirrus_but_not_in_mysql(cirrus_df: DataFrame, mysql_df: DataFrame) -> DataFrame:
    """
    pages that are in the cirrus index but not in the mysql page table.

    The cirrus_df data is expected to be from a more recent state than the mysql_df one.
    The cirrus_df is filtered to exclude all pages that have been added/modified after
    the cutoff date taken from the mysql_df state.

    :param cirrus_df: the cirrus dataset
    :param mysql_df: the page table
    :return: a filtered dataset of pages present in the cirrus index
    """

    cutoff_dates_df = cutoff_dates(mysql_df)
    return (cirrus_df
            .join(cutoff_dates_df, 'wiki')
            .filter(cirrus_df['timestamp'] < cutoff_dates_df['max_timestamp'])
            .join(mysql_df, ['page_id', 'wiki'], "leftanti")
            .withColumn("inconsistency_type", lit("in_cirrus_but_not_in_mysql"))
            .drop(col("max_timestamp")))


def in_mysql_but_not_in_cirrus(cirrus_df: DataFrame, mysql_df: DataFrame) -> DataFrame:
    """
    pages that are in mysql but not in the cirrus index.

    The mysql_df dataset is expected to be from more recent state than the cirrus_df one.
    The cirrus_df is filtered to exclude all pages that have been added/modified after
    the cutoff date taken from the cirrus_df state.

    :param cirrus_df: the cirrus dataset
    :param mysql_df: the page table
    :return: a filtered data of pages present in mysql but not in cirrus.
    """
    cutoff_dates_df = cutoff_dates(cirrus_df)
    return (mysql_df
            .join(cutoff_dates_df, 'wiki', 'inner')
            .filter(mysql_df['timestamp'] < cutoff_dates_df['max_timestamp'])
            .filter(mysql_df['page_is_redirect'] == lit(False))
            .join(cirrus_df, ['page_id', 'wiki'], "leftanti")
            .withColumn("inconsistency_type", lit("in_mysql_but_not_in_cirrus"))
            .drop("page_is_redirect", "max_timestamp"))


def redirect_in_cirrus(cirrus_df: DataFrame, mysql_df: DataFrame) -> DataFrame:
    """
    pages marked as redirects in the page table but present in cirrus.

    The cirrus_df data is expected to be from a more recent state than the mysql_df one.
    The cirrus_df is filtered to exclude all pages that have been added/modified after
    the cutoff date taken from the mysql_df state.

    This check is likely to produce some false positives as it relies on the `page_is_redirect` field
    which we know might not reflect the actual status of the page as interpreted by the content.
    This is particularly true for javascript & css content_models where the redirection is detected
    by a regular expression on the page content.

    :param cirrus_df: the cirrus dataset
    :param mysql_df: the page table
    :return: a filtered dataset of pages marked as redirects but present in cirrus.
    """
    cutoff_dates_df = cutoff_dates(mysql_df)
    mysql_df_redirects = mysql_df.filter(mysql_df['page_is_redirect'] == lit(True)).alias("mysql")

    return (cirrus_df.alias("cirrus")
            .join(cutoff_dates_df, 'wiki', 'inner')
            .join(mysql_df_redirects, ['page_id', 'wiki'], "inner")
            .filter(cirrus_df['timestamp'] < cutoff_dates_df['max_timestamp'])
            .select("page_id", "wiki", "mysql.*")
            .withColumn("inconsistency_type", lit("redirect_in_cirrus"))
            .drop("max_timestamp", "page_is_redirect"))


def revision_mismatch(cirrus_df: DataFrame, mysql_df: DataFrame) -> DataFrame:
    """
    pages that are present in both datasets but with a different revision

    The cirrus_df data is expected to be from a more recent state than the mysql_df one.
    The cirrus_df is filtered to exclude all pages that have been added/modified after
    the cutoff date taken from the mysql_df state.

    :param cirrus_df: the cirrus dataset
    :param mysql_df: the page table
    :return: a filtered dataset of pages marked as redirects but present in cirrus.
    """
    cutoff_dates_df = cutoff_dates(mysql_df)
    return (cirrus_df.alias("cirrus")
            .join(cutoff_dates_df, 'wiki')
            .filter(cirrus_df['timestamp'] < cutoff_dates_df['max_timestamp'])
            .join(mysql_df.alias("mysql"), ['page_id', 'wiki'], "inner")
            .filter(mysql_df['revision'] > cirrus_df['revision'])
            .select("page_id", "wiki", "mysql.*")
            .withColumn("inconsistency_type", lit("revision_mismatch"))
            .drop("page_is_redirect", "max_timestamp"))


def aggregate(inconsistencies: DataFrame, replica: str) -> DataFrame:
    """
    aggregates all the data per type of inconsistency and content_model
    :param inconsistencies: all the inconsistencies
    :param replica: the cirrus_replica to work with
    :return: the aggregated dataframe
    """
    return (inconsistencies.groupBy('inconsistency_type', 'content_model')
            .agg(count(lit(1)).alias('count'))
            .withColumn('cirrus_replica', lit(replica)))


def cutoff_dates(df: DataFrame) -> DataFrame:
    """
    Extract the cutoff date of the snapshot, this a rough estimation of the "age" of the dataset.
    This helps to ignore data that is likely to have been modified after this date and make sure
    that the two subsets we compare are "equivalent".
    """
    return (df.withColumn("timestamp", unix_timestamp(col("timestamp")))
            .groupBy("wiki")
            .agg(_max("timestamp").alias("max_timestamp"))
            .withColumn("max_timestamp",
                        from_unixtime(col("max_timestamp") - lit(DUMP_TIME_ADJ.total_seconds()))))


def run_cli() -> int:
    logging.basicConfig(level=logging.INFO)
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == "__main__":
    sys.exit(run_cli())
