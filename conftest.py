from __future__ import absolute_import

import json
import logging
import os
from pyspark.sql import SparkSession, types as T
import pytest


def quiet_log4j():
    logger = logging.getLogger('py4j')
    logger.setLevel(logging.WARN)


@pytest.fixture(scope="session")
def spark():
    quiet_log4j()

    builder = (
        SparkSession.builder
        .master('local[2]')
        .appName('pytest-pyspark-local-testing')
        .config('spark.sql.shuffle.partitions', 4)
    )
    if 'XDG_CACHE_HOME' in os.environ:
        builder.config('spark.jars.ivy', os.path.join(os.environ['XDG_CACHE_HOME'], 'ivy2'))

    with builder.getOrCreate() as spark:
        yield spark


@pytest.fixture
def fixture_dir():
    return os.path.join(os.path.dirname(__file__), 'tests', 'fixtures')


def create_df_fixture(df):
    """Create a fixture from a dataframe

    Not actually used in the test suite, but included as documentation for how
    to create the files used by get_df_fixture. Don't put real PII in fixtures.
    """
    return {
        # This reutrns a string, but we will need the decoded
        # structure when loading so no reason to double encode.
        'schema': json.loads(df.schema.json()),
        'rows': [[getattr(row, field) for field in df.columns] for row in df.collect()],
    }


@pytest.fixture
def get_fixture(fixture_dir):
    def fn(group, name):
        with open(os.path.join(fixture_dir, group, name), 'rt') as f:
            return f.read()
    return fn


@pytest.fixture
def get_df_fixture(spark, get_fixture):
    def fn(group, name):
        raw_data = json.loads(get_fixture(group, name + '.json'))
        schema = T.StructType.fromJson(raw_data['schema'])
        return spark.createDataFrame(raw_data['rows'], schema)
    return fn
