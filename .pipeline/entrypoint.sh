#!/bin/sh

# if run as root, assume we are invoked from a linux command line
# that mounted /srv/app from the host and wants to adjust the somebody
# user to match that uid to make permissions happy.
if [ "$(id -u)" -eq 0 ]; then
    echo "Updating system to use external uid/gid of $(stat --format=%u:%g .)" 
    usermod -u "$(stat --format=%u .)" somebody
    groupmod -g "$(stat --format=%g .)" somebody
    echo "Running pipeline"
    gosu somebody:somebody "$0" "$@"
    exit $?
fi

if [ "$1" = "bash" ]; then
    # little hax...we know bash_aliases isn't configured in the docker container,
    # so put our setup script there and it will be sourced by the default .bashrc
    cp "$(dirname $0)/setup.sh" ~/.bash_aliases
    # never returns
    exec bash
fi

 . $(dirname $0)/setup.sh
python -m tox "$@"
