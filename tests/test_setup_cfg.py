from configparser import ConfigParser
import importlib
from pathlib import Path
import pytest


setup_cfg_path = Path(__file__).parents[1].joinpath('setup.cfg')
config = ConfigParser()
config.read(str(setup_cfg_path))


def get_multiline_kv(section, option):
    value = config.get(section, option)
    for line in value.split('\n'):
        if not len(line):
            continue
        k, v = line.split('=', 1)
        yield k.strip(), v.strip()


@pytest.mark.parametrize('name, call_spec',
                         get_multiline_kv('options.entry_points', 'console_scripts'))
def test_console_script_exists(name, call_spec):
    module_name, func_name = call_spec.split(':', 1)
    module = importlib.import_module(module_name)
    assert hasattr(module, func_name), name
