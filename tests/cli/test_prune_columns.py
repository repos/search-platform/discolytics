from typing import List, Any

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, LongType, StringType, ArrayType, MapType

import discolytics.cli.prune_columns as cli
from discolytics.cli.prune_columns import CONVERSIONS
from discolytics.hive import HivePartitionWriter

FAKE_PARTIAL_CIRRUS_SCHEMA = StructType(fields=[
    StructField('page_id', LongType(), True),
    StructField('text', StringType(), True),
    StructField('source_text', StringType(), True),
    StructField('auxiliary_text', ArrayType(StringType()), True),
    StructField('opening_text', StringType(), True),
    StructField('outgoing_link', ArrayType(StringType()), True),
    StructField('external_link', ArrayType(StringType()), True),
    StructField('file_text', StringType(), True),
    StructField('labels', MapType(StringType(), StringType()), True),
    StructField('descriptions', MapType(StringType(), StringType()), True),
    StructField('statement_keywords', ArrayType(StringType()), True),
    StructField('extra_source', StringType(), True),
    StructField('cirrus_replica', StringType(), False),
    StructField('cirrus_group', StringType(), False),
    StructField('wiki', StringType(), False),
    StructField('snapshot', StringType(), True)
])

TEST_DATA: List[Any] = [
    {
        'page_id': 1,
        'text': 'data',
        'source_text': 'data',
        'auxiliary_text': ['data'],
        'opening_text': 'data',
        'outgoing_link': ['data'],
        'file_text': 'data',
        'labels': {'mul': 'data'},
        'descriptions': {'mul': 'data'},
        'statement_keywords': ['data'],
        'extra_source': 'data',
        'cirrus_replica': 'replica1',
        'cirrus_group': 'group1',
        'snapshot': '20230101',
        'wiki': 'mywiki'
    }
]

EXPECTED_DATA = [
    {
        'page_id': 1,
        'outgoing_link': ['data'],
        'cirrus_replica': 'replica1',
        'cirrus_group': 'group1',
        'snapshot': '20230101',
        'wiki': 'mywiki'
    }
]


def test_drop_conflicting_source_partition_fields(spark: SparkSession):
    output_partition = HivePartitionWriter.from_spec(
        "source_table/cirrus_replica=test/snapshot=20230101")
    df = spark.createDataFrame(TEST_DATA, FAKE_PARTIAL_CIRRUS_SCHEMA)  #
    df = cli.drop_conflicting_source_partition_fields(output_partition, df)
    fields = [f.name for f in df.schema.fields]
    assert 'cirrus_replica' not in fields
    assert 'snapshot' not in fields


def test_filter_content_fields(spark: SparkSession):
    df = spark.createDataFrame(TEST_DATA, FAKE_PARTIAL_CIRRUS_SCHEMA)
    removed_fields = CONVERSIONS['cirrus_index_without_content'].removed_fields
    df = cli.filter_content_fields(df, removed_fields)
    assert [r.asDict() for r in df.collect()] == EXPECTED_DATA
