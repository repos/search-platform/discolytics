from discolytics.cli.webrequest_metrics import webrequest_metrics
from pyspark.sql import types as T
import pytest


# Dupe of prod schema reported by spark.
SCHEMA = T.StructType([
    T.StructField("hostname", T.StringType()),
    T.StructField("sequence", T.LongType()),
    T.StructField("dt", T.StringType()),
    T.StructField("time_firstbyte", T.DoubleType()),
    T.StructField("ip", T.StringType()),
    T.StructField("cache_status", T.StringType()),
    T.StructField("http_status", T.StringType()),
    T.StructField("response_size", T.LongType()),
    T.StructField("http_method", T.StringType()),
    T.StructField("uri_host", T.StringType()),
    T.StructField("uri_path", T.StringType()),
    T.StructField("uri_query", T.StringType()),
    T.StructField("content_type", T.StringType()),
    T.StructField("referer", T.StringType()),
    T.StructField("x_forwarded_for", T.StringType()),
    T.StructField("user_agent", T.StringType()),
    T.StructField("accept_language", T.StringType()),
    T.StructField("x_analytics", T.StringType()),
    T.StructField("range", T.StringType()),
    T.StructField("is_pageview", T.BooleanType()),
    T.StructField("record_version", T.StringType()),
    T.StructField("client_ip", T.StringType()),
    T.StructField("geocoded_data", T.MapType(T.StringType(), T.StringType())),
    T.StructField("x_cache", T.StringType()),
    T.StructField("user_agent_map", T.MapType(T.StringType(), T.StringType())),
    T.StructField("x_analytics_map", T.MapType(T.StringType(), T.StringType())),
    T.StructField("ts", T.TimestampType()),
    T.StructField("access_method", T.StringType()),
    T.StructField("agent_type", T.StringType()),
    T.StructField("is_zero", T.BooleanType()),
    T.StructField("referer_class", T.StringType()),
    T.StructField("normalized_host", T.StructType([
        T.StructField("project_class", T.StringType()),
        T.StructField("project", T.StringType()),
        T.StructField("qualifiers", T.ArrayType(T.StringType())),
        T.StructField("tld", T.StringType()),
        T.StructField("project_family", T.StringType()),
    ])),
    T.StructField("pageview_info", T.MapType(T.StringType(), T.StringType())),
    T.StructField("page_id", T.LongType()),
    T.StructField("namespace_id", T.IntegerType()),
    T.StructField("tags", T.ArrayType(T.StringType())),
    T.StructField("isp_data", T.MapType(T.StringType(), T.StringType())),
    T.StructField("accept", T.StringType()),
    T.StructField("tls", T.StringType()),
    T.StructField("tls_map", T.MapType(T.StringType(), T.StringType())),
    T.StructField("ch_ua", T.StringType()),
    T.StructField("ch_ua_mobile", T.StringType()),
    T.StructField("ch_ua_platform", T.StringType()),
    T.StructField("ch_ua_arch", T.StringType()),
    T.StructField("ch_ua_bitness", T.StringType()),
    T.StructField("ch_ua_full_version_list", T.StringType()),
    T.StructField("ch_ua_model", T.StringType()),
    T.StructField("ch_ua_platform_version", T.StringType()),
    T.StructField("referer_data", T.StructType([
        T.StructField("referer_class", T.StringType()),
        T.StructField("referer_name", T.StringType())
    ])),
    T.StructField("webrequest_source", T.StringType()),
    T.StructField("year", T.IntegerType()),
    T.StructField("month", T.IntegerType()),
    T.StructField("day", T.IntegerType()),
    T.StructField("hour", T.IntegerType()),
])


@pytest.fixture
def eval_row(spark):
    def fn(row_dict):
        rows = (
            spark.createDataFrame([row_dict], schema=SCHEMA)
            .select(*(metric.alias(name) for name, metric in webrequest_metrics().items()))
            .collect()
        )
        return rows[0]
    return fn


test_data = {
    'generic pageview': (
        {'num_pageviews'},
        {
            'is_pageview': True,
        }),
    'desktop autocomplete': (
        {'num_autocomplete_pv', 'num_search_pv', 'num_discovery_pv'},
        {
            'http_status': '302',
            'x_analytics_map': {
                'wprov': 'acrw1_0',
            },
        }),
    'mobile autocomplete': (
        {'num_autocomplete_pv', 'num_search_pv', 'num_discovery_pv', 'num_pageviews'},
        {
            'is_pageview': True,
            'referer': 'https://example.wiki/wiki/Example?searchToken=abc123'
        }),
    'Pageview from Special:Search': (
        {'num_special_search_pv', 'num_search_pv', 'num_discovery_pv', 'num_pageviews'},
        {
            'is_pageview': True,
            'x_analytics_map': {
                'wprov': 'srpw1_0',
            }
        }),
    'Pageview from related articles': (
        {'num_related_articles_pv', 'num_discovery_pv', 'num_pageviews'},
        {
            'is_pageview': True,
            'x_analytics_map': {
                'wprov': 'rarw1',
            }
        }),
    'Pageview from external search engine': (
        {'num_external_search_pv', 'num_pageviews'},
        {
            'is_pageview': True,
            'referer_class': 'external (search engine)'
        }),
    'Internally referred page views': (
        {'num_internal_pv', 'num_pageviews'},
        {
            'is_pageview': True,
            'referer_class': 'internal',
        }),
}


@pytest.mark.parametrize('expect,row', test_data.values(), ids=test_data.keys())
def test_special_search_req(eval_row, expect, row):
    result = eval_row(row)
    for k in webrequest_metrics().keys():
        if k in expect:
            # Note that the names are intended for aggregation, 'num' is a boolean because it hasn't
            # been summed yet.
            assert result[k] is True, k
        else:
            assert result[k] in (False, None), k
