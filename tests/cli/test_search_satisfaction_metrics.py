from discolytics.cli.search_satisfaction_metrics import (
        agg_from_events, reporting_dimensions, satisfaction_metrics)

from pyspark.sql import types as T
import pytest


# Minimal copy of prod schema
SCHEMA = T.StructType([
    T.StructField("normalized_host", T.StructType([
        T.StructField("project_class", T.StringType()),
        T.StructField("project", T.StringType()),
        T.StructField("qualifiers", T.ArrayType(T.StringType())),
        T.StructField("tld", T.StringType()),
        T.StructField("project_family", T.StringType()),
    ])),
    T.StructField("useragent", T.StructType([
        T.StructField("is_bot", T.BooleanType()),
        T.StructField("os_family", T.StringType()),
        T.StructField("browser_family", T.StringType()),
    ])),
    T.StructField("geocoded_data", T.StructType([
        T.StructField("country", T.StringType()),
    ])),
    T.StructField("event", T.StructType([
        T.StructField("source", T.StringType()),
        T.StructField("action", T.StringType()),
        T.StructField("position", T.IntegerType()),
        T.StructField("checkin", T.IntegerType()),
        T.StructField("searchSessionId", T.StringType()),
        T.StructField("pageViewId", T.StringType()),
        T.StructField("userEditBucket", T.StringType()),
        T.StructField("hitsReturned", T.IntegerType()),
    ])),
    T.StructField("year", T.IntegerType()),
    T.StructField("month", T.IntegerType()),
    T.StructField("day", T.IntegerType()),
    T.StructField("hour", T.IntegerType()),
])


@pytest.fixture
def eval_row(spark):
    def fn(row_dict):
        rows = (
            spark.createDataFrame([row_dict], schema=SCHEMA)
            .select(*(metric.alias(name) for name, metric in satisfaction_metrics().items()))
            .collect()
        )
        return rows[0]
    return fn


test_data = {
    'fulltext serp': (
        {'fulltext_serp'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'searchResultPage',
            },
        }),
    'fulltext serp_without_results': (
        {'fulltext_serp'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'searchResultPage',
                'hitsReturned': 0,
            },
        }),
    'fulltext serp_w_results': (
        {'fulltext_serp', 'fulltext_serp_w_results'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'searchResultPage',
                'hitsReturned': 1,
            },
        }),
    'fulltext serp via click': (
        {'fulltext_serp', 'fulltext_serp_w_results', 'fulltext_click'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'click',
            },
        }),
    'fullltext visit dsat': (
        {'fulltext_visit'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'checkin',
                'checkin': 0,
            }
        }
    ),
    'fulltext visit sat': (
        {'fulltext_visit', 'fulltext_visit_sat'},
        {
            'event': {
                'source': 'fulltext',
                'action': 'checkin',
                'checkin': 10,
            }
        }
    ),
    'fulltext_serp_w_virtual_pv': (
        {'fulltext_serp_w_virtual_pv'},
        {
            'source': 'fulltext',
            'action': 'virtualpageview',
        }
    ),

    'autocomplete serp': (
        {'autocomplete_serp'},
        {
            'event': {
                'source': 'autocomplete',
                'action': 'searchResultPage',
            }
        }),

    'autocomplete self-typed submit': (
        {'autocomplete_serp', 'autocomplete_submit'},
        {
            'event': {
                'source': 'autocomplete',
                'action': 'click',
                'position': -1,
            }
        }),

    'autocomplete provided submit': (
        {'autocomplete_serp', 'autocomplete_submit', 'autocomplete_success'},
        {
            'event': {
                'source': 'autocomplete',
                'action': 'click',
                'position': 1,
            }
        }),

}


@pytest.mark.parametrize('expect,row', test_data.values(), ids=test_data.keys())
def test_base_metrics(eval_row, expect, row):
    result = eval_row(row)
    for k in satisfaction_metrics().keys():
        if k in expect:
            assert result[k] is True, k
        else:
            assert result[k] in (False, None), k


def test_output_metric_names(spark):
    # Gives a place to verify (manually) that SCHEMA represents the set of
    # columns we require, and the columns going out of the transformation match
    # the columns in the hive schema.
    # Additionally, by running the full transformation, makes sure column names
    # are coherent across the transformation.
    df_in = spark.createDataFrame([], schema=SCHEMA)
    df_out = agg_from_events(df_in, reporting_dimensions())
    result = df_out.collect()

    assert result == []
    assert set(df_out.columns) == {
            'os_family',
            'country',
            'browser_family',
            'user_edit_bucket',
            'access_method',
            'normalized_host',

            'num_fulltext_serp',
            'num_fulltext_serp_w_results',
            'num_fulltext_serp_w_click',
            'num_fulltext_visit_sat',
            'num_fulltext_serp_w_virtual_pv',

            'num_sessions_w_fulltext_serp',
            'num_sessions_w_fulltext_serp_w_results',
            'num_sessions_w_fulltext_serp_w_click',
            'num_sessions_w_fulltext_visit_sat',
            'num_sessions_w_fulltext_serp_w_virtual_pv',

            'num_autocomplete_serp',
            'num_autocomplete_submit',
            'num_autocomplete_success',
            'num_autocomplete_abandon',

            'num_sessions_w_autocomplete_serp',
            'num_sessions_w_autocomplete_submit',
            'num_sessions_w_autocomplete_success',
            'num_sessions_w_autocomplete_abandon',

            'num_sessions_w_fulltext_dsat',
            'num_sessions_w_fulltext_abandon',
            'num_sessions_w_fulltext_abandon_and_virtual_pv',
    }
