from datetime import datetime
from discolytics.cli import drop_dated_directories as ddd
import pytest
from unittest.mock import Mock


@pytest.mark.parametrize('should_prune,base_name,reason', [
    (True, 'date=20230101', 'Valid date passed with date= prefix'),
    (True, '20230203', 'Valid date passed with bare prefix'),
    (False, 'date=20240102', 'Date is after prune_before'),
    (False, '12345678', 'Invalid date passed'),
    (False, 'date=20232020', 'Invalid date passed'),
    (False, 'qwerty', 'Invalid date passed'),
])
def test_subdirs_to_prune(should_prune, base_name, reason):
    prune_before = datetime(2024, 1, 1)

    subdir = Mock()
    subdir.file_info.base_name = base_name
    subdir.uri = f'hdfs://pytest/path/to/{base_name}'

    source = Mock()
    source.subdirs.return_value = [subdir]

    to_prune = list(ddd.subdirs_to_prune(source, prune_before))
    if should_prune:
        assert len(to_prune) == 1, reason
        assert to_prune[0] is subdir, reason
    else:
        assert len(to_prune) == 0, reason
