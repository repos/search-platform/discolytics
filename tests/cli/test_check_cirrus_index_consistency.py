from datetime import datetime
from typing import Mapping, Iterable, Callable

import pytest
from prometheus_client import CollectorRegistry
from pyspark import Row
from pyspark.sql import types as T

import discolytics.cli.check_cirrus_index_consistency as cli

PAGE_TABLE_SCHEMA = T.StructType([
    T.StructField("page_id", T.LongType(), True),
    T.StructField("page_latest", T.LongType(), True),
    T.StructField("page_namespace", T.IntegerType(), True),
    T.StructField("page_is_redirect", T.BooleanType(), True),
    T.StructField("page_touched", T.StringType(), True),
    T.StructField("page_links_updated", T.StringType(), True),
    T.StructField("page_content_model", T.StringType(), True),
    T.StructField("wiki_db", T.StringType(), True),
])

CIRRUS_SCHEMA = T.StructType([
    T.StructField('page_id', T.LongType(), True),
    T.StructField('content_model', T.StringType(), True),
    T.StructField('namespace', T.LongType(), True),
    T.StructField('timestamp', T.StringType(), True),
    T.StructField('version', T.LongType(), True),
    T.StructField('wiki', T.StringType(), True),
])

INCONSISTENCIES_SCHEMA = T.StructType([
    T.StructField('wiki', T.StringType(), True),
    T.StructField('page_id', T.LongType(), True),
    T.StructField('content_model', T.StringType(), True),
    T.StructField('namespace', T.IntegerType(), True),
    T.StructField('revision', T.LongType(), True),
    T.StructField('timestamp', T.TimestampType(), True),
    T.StructField('inconsistency_type', T.StringType(), True),
    T.StructField('num_pages', T.LongType(), True),
    T.StructField('num_redirects', T.LongType(), True),
])

INCONSISTENCIES_STAT_SCHEMA = T.StructType([
    T.StructField('wiki', T.StringType(), True),
    T.StructField('snapshot', T.TimestampType(), True),
    T.StructField('inconsistency_type', T.StringType(), True),
    T.StructField('cirrus_replica', T.StringType(), True),
    T.StructField('count', T.LongType(), True),
])


@pytest.mark.parametrize(
    argnames=['page_table_data', 'cirrus_data', 'expected_rows', 'detector'],
    argvalues=[
        (
                [
                    {
                        "page_id": 1,
                        "page_latest": 123,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230201000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # unrelated wiki, make sure that we join on page_id, wiki
                        "page_id": 2,
                        "page_latest": 123,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "unrelatedwiki"
                    }
                ],
                [
                    {  # common page
                        "page_id": 1,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-02-01T00:00:00Z",
                        "version": 123,
                        "wiki": "testwiki"
                    },
                    {  # only in cirrus
                        "page_id": 2,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 223,
                        "wiki": "testwiki"
                    },
                    {  # only in cirrus but ignored because appeared after snapshot_time
                        "page_id": 3,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-02-02T00:00:00Z",
                        "version": 323,
                        "wiki": "testwiki"
                    }
                ],
                [Row(page_id=2, wiki='testwiki', namespace=0, revision=223,
                     timestamp=datetime(2023, 1, 1, 0, 0), content_model='wikitext',
                     inconsistency_type='in_cirrus_but_not_in_mysql')],
                cli.in_cirrus_but_not_in_mysql
        ),
        (
                [
                    {  # common
                        "page_id": 1,
                        "page_latest": 123,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230201000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # a redirect
                        "page_id": 2,
                        "page_latest": 223,
                        "page_namespace": 0,
                        "page_is_redirect": True,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # only in mysql but ignored because appeared after the snapshot_time
                        "page_id": 3,
                        "page_latest": 323,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230201000000",
                        "page_links_updated": "20230201000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # only in mysql
                        "page_id": 4,
                        "page_latest": 423,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },

                ],
                [
                    {  # common page
                        "page_id": 1,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-02-01T00:00:00Z",
                        "version": 123,
                        "wiki": "testwiki"
                    },
                    {  # unrelated wiki, make sure that we join on page_id, wiki
                        "page_id": 4,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 123,
                        "wiki": "unrelatedwiki"
                    }
                ],
                [Row(page_id=4, wiki='testwiki', namespace=0, revision=423,
                     timestamp=datetime(2023, 1, 1, 0, 0), content_model='wikitext',
                     inconsistency_type='in_mysql_but_not_in_cirrus')],
                cli.in_mysql_but_not_in_cirrus
        ),
        (
                [
                    {  # page missing in cirrus
                        "page_id": 1,
                        "page_latest": 123,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # a redirect present in cirrus
                        "page_id": 2,
                        "page_latest": 223,
                        "page_namespace": 0,
                        "page_is_redirect": True,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # a redirect not in cirrus
                        "page_id": 3,
                        "page_latest": 323,
                        "page_namespace": 0,
                        "page_is_redirect": True,
                        "page_touched": "20230102000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # a redirect in cirrus but whose timestamp is too recent so ignored
                        "page_id": 4,
                        "page_latest": 423,
                        "page_namespace": 0,
                        "page_is_redirect": True,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                ],
                [
                    {  # redirect in cirrus
                        "page_id": 2,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 223,
                        "wiki": "testwiki"
                    },
                    {  # unrelated wiki, make sure that we join on page_id, wiki
                        "page_id": 2,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 223,
                        "wiki": "unrelatedwiki"
                    },
                    {
                        "page_id": 4,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-02T00:00:00Z",
                        "version": 424,
                        "wiki": "testwiki"
                    }
                ],
                [Row(page_id=2, wiki='testwiki', namespace=0, revision=223,
                     timestamp=datetime(2023, 1, 1, 0, 0), content_model='wikitext',
                     inconsistency_type='redirect_in_cirrus')],
                cli.redirect_in_cirrus
        ),
        (
                [
                    {  # common page but with revision mismatch
                        "page_id": 1,
                        "page_latest": 124,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # common page
                        "page_id": 2,
                        "page_latest": 223,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # mismatch ignored because appeared after the snapshot_time
                        "page_id": 3,
                        "page_latest": 324,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230201000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },
                    {  # only in mysql
                        "page_id": 4,
                        "page_latest": 424,
                        "page_namespace": 0,
                        "page_is_redirect": False,
                        "page_touched": "20230101000000",
                        "page_links_updated": "20230101000000",
                        "page_content_model": "wikitext",
                        "wiki_db": "testwiki"
                    },

                ],
                [
                    {  # common page but with revision mismatch
                        "page_id": 1,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 123,
                        "wiki": "testwiki"
                    },
                    {  # common page
                        "page_id": 2,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-02T00:00:00Z",
                        "version": 223,
                        "wiki": "testwiki"
                    },
                    {  # mismatch ignored because of timestamp
                        "page_id": 3,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-02-01T00:00:00Z",
                        "version": 323,
                        "wiki": "testwiki"
                    },
                    {  # unrelated wiki, make sure that we join on page_id, wiki
                        "page_id": 4,
                        "content_model": "wikitext",
                        "namespace": 0,
                        "timestamp": "2023-01-01T00:00:00Z",
                        "version": 423,
                        "wiki": "unrelatedwiki"
                    }
                ],
                [Row(page_id=1, wiki='testwiki', namespace=0, revision=124,
                     timestamp=datetime(2023, 1, 1, 0, 0), content_model='wikitext',
                     inconsistency_type='revision_mismatch')],
                cli.revision_mismatch
        )
    ]
)
def test_inconsistency_detection(spark, page_table_data: Mapping, cirrus_data: Mapping,
                                 expected_rows: Iterable[Row],
                                 detector: Callable):
    page_table_df = spark.createDataFrame(page_table_data, PAGE_TABLE_SCHEMA)
    cirrus_df = spark.createDataFrame(cirrus_data, CIRRUS_SCHEMA)

    page_table_df = cli.select_page_table_fields(page_table_df)
    cirrus_df = cli.select_cirrus_fields(cirrus_df)
    rows = detector(cirrus_df, page_table_df).collect()
    assert rows == expected_rows


def test_page_counts(spark):
    mandatory_columns = {
        'page_id': 1,
        'page_namespace': 0,
        'page_touched': '19700101000000',
        'page_latest': 123
    }
    data = [
        {'wiki_db': 'wiki1', 'page_content_model': 'model1', 'page_is_redirect': False},
        {'wiki_db': 'wiki1', 'page_content_model': 'model2', 'page_is_redirect': False},
        {'wiki_db': 'wiki1', 'page_content_model': 'model1', 'page_is_redirect': True},
        {'wiki_db': 'wiki2', 'page_content_model': 'model1', 'page_is_redirect': False},
        {'wiki_db': 'wiki2', 'page_content_model': 'model1', 'page_is_redirect': True},
        {'wiki_db': 'wiki2', 'page_content_model': 'model2', 'page_is_redirect': True},
    ]
    for d in data:
        d.update(mandatory_columns)
    df = spark.createDataFrame(data, PAGE_TABLE_SCHEMA)
    counts = cli.page_counts_per_wiki_and_content_model(cli.select_page_table_fields(df))
    expected_counts = [
        {'wiki': 'wiki1', 'content_model': 'model1', 'num_pages': 1, 'num_redirects': 1},
        {'wiki': 'wiki1', 'content_model': 'model2', 'num_pages': 1, 'num_redirects': 0},
        {'wiki': 'wiki2', 'content_model': 'model1', 'num_pages': 1, 'num_redirects': 1},
        {'wiki': 'wiki2', 'content_model': 'model2', 'num_pages': 0, 'num_redirects': 1},
    ]

    def key(r: Mapping):
        return r['wiki'] + r['content_model']

    assert sorted([r.asDict() for r in counts.collect()], key=key) == \
           sorted(expected_counts, key=key)


def test_aggregate(spark):
    data = [
        *[{'wiki': 'wiki1', 'content_model': 'wikitext',
           'inconsistency_type': 'revision_mismatch'}] * 3,
        *[{'wiki': 'wiki1', 'content_model': 'wikitext',
           'inconsistency_type': 'in_mysql_but_not_in_cirrus'}] * 4,
        *[{'wiki': 'wiki1', 'content_model': 'wikitext',
           'inconsistency_type': 'in_cirrus_but_not_in_mysql'}] * 5,
        *[{'wiki': 'wiki1', 'content_model': 'wikitext',
           'inconsistency_type': 'redirect_in_cirrus'}] * 6,
    ]
    df = spark.createDataFrame(data, INCONSISTENCIES_SCHEMA)
    actual_df = cli.aggregate(df, 'replica1')
    expected_rows = [
        {'content_model': 'wikitext', 'inconsistency_type': 'revision_mismatch',
         'cirrus_replica': 'replica1', 'count': 3},
        {'content_model': 'wikitext', 'inconsistency_type': 'in_mysql_but_not_in_cirrus',
         'cirrus_replica': 'replica1', 'count': 4},
        {'content_model': 'wikitext', 'inconsistency_type': 'in_cirrus_but_not_in_mysql',
         'cirrus_replica': 'replica1', 'count': 5},
        {'content_model': 'wikitext', 'inconsistency_type': 'redirect_in_cirrus',
         'cirrus_replica': 'replica1', 'count': 6},
    ]

    def key(r: Mapping):
        return r['content_model'] + r['inconsistency_type']

    assert sorted([r.asDict() for r in actual_df.collect()], key=key) == \
           sorted(expected_rows, key=key)


def test_collect_metrics(spark):
    df = spark.createDataFrame([
        {'inconsistency_type': 'in_cirrus_but_not_in_mysql',
         'cirrus_replica': 'replica', 'content_model': 'wikitext', 'count': 123},
        {'inconsistency_type': 'redirect_in_cirrus',
         'cirrus_replica': 'replica', 'content_model': 'wikitext', 'count': 124},
    ])
    registry = CollectorRegistry()
    cli.collect_metrics(df, registry)

    assert registry.get_sample_value('cirrussearch_content_inconsistency',
                                     {'inconsistency_type': 'in_cirrus_but_not_in_mysql',
                                      'content_model': 'wikitext',
                                      'cirrus_replica': 'replica'}) == 123.0
    assert registry.get_sample_value('cirrussearch_content_inconsistency',
                                     {'inconsistency_type': 'redirect_in_cirrus',
                                      'content_model': 'wikitext',
                                      'cirrus_replica': 'replica'}) == 124.0
